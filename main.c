#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>

#include "QuickMerge.h"

#define ROOT 0

/*
	FELIPE XAVIER  05/07/2018
	IMPLEMENTAÇÃO DO ALGORITMO QUICKMERGE
*/
int main(int argc, char *argv[])
{
	Processo *processo = malloc(sizeof(Processo)); 

	if(argc != 2)
	{
		printf("ERRO: Informe o nome do arquivo.\n");
		return -1;
	}
	else
		processo->sArquivo = argv[1];

	MPI_Init(NULL, NULL);				

	MPI_Comm_size(MPI_COMM_WORLD, &processo->comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &processo->my_rank);

	processo->Comm = MPI_COMM_WORLD;
	processo->root_rank = ROOT;
	
	processo->fatorMemoria = processo->comm_sz;

	quickMerge(processo);

	MPI_Finalize();

    deleteProcesso(processo);

	return 0;
}