
#include "QuickMerge.h"

// ALOCA MEMÓRIA PARA AS LISTAS DE ELEMENTOS
int alocaMemoria(Processo *processo)
{
    /* 

    O IDEAL SERIA QUE CADA PROCESSO TIVESSE N ELEMENTOS PARA ORDENAR DURANTE TODA EXECUÇÃO
    N = QUANTIDADE TOTAL DE ELEMENTOS/QUANTIDADE DE PROCESSOS

    * PORÉM, PODE ACONTECER DURANTE O ORDENAMENTO DE ALGUNS PROCESSOS FICAREM COM MAIS ELEMENTOS PARA 
    ORDENAR DO QUE OUTROS PROCESSOS

    ** O USO DO REALLOC NAO DEU CERTO PARA UMA GRANDE QUANTIDADE DE ELEMENTOS

    *** POR ISSO É ALOCADO MAIS MEMÓRIA DO QUE NECESSÁRIO INICIALMENTE

    */

    processo->sizeList = processo->fatorMemoria*processo->elementosPorProcesso;
    processo->sizeHighList = (int) ceil(processo->sizeList/2);
    processo->sizeLowList = processo->sizeHighList;
    processo->vetorTempLength = processo->sizeHighList;

    processo->list = (int *) malloc(processo->sizeList * sizeof(int));
    processo->lowList = (int *) malloc(processo->sizeLowList * sizeof(int));
    processo->highList = (int *) malloc(processo->sizeHighList * sizeof(int)); 
    processo->vetorTemp = (int *) malloc(processo->vetorTempLength * sizeof(int));

    return +1;
}

void deleteProcesso(Processo *processo)
{
    free(processo->list);
    free(processo->lowList);
    free(processo->highList);
    free(processo->vetorTemp);
    free(processo);
}

void salvarArquivo(Processo *processo)
{
    if(processo->lengthList <= 0)
        return;

    FILE *file;
    char file_name[10];
    sprintf(file_name,"rank_%d.dat", processo->my_rank );

    int i = 0;

    if( (file = fopen(file_name, "wb")) == NULL)
    {
        printf("ERRO: Arquivo não pode ser aberto (processo %d)\n", processo->my_rank);
        return;
    }

    for(i = 0; i < processo->lengthList; i++)
    {
        if( fwrite(&processo->list[i], sizeof(int), 1, file) != 1 )
            printf("Erro de escrita do arquivo %s", file_name);
    }

    fclose(file);
}

//RETORNA NOME DO ARQUIVO QUE PRECISA SER ORDENADO
char *getNomeArquivo()
{
    struct dirent *de;   
  
    // DIRETORIO DO PROGRAMA
    DIR *dr = opendir("."); 
  
    if (dr == NULL)  
        return NULL; 
  
    while ((de = readdir(dr)) != NULL) 
    { 
        if(strstr(de->d_name, "ordenacao_") != NULL)
            return de->d_name;
    }
    
    closedir(dr); //FECHA DIRETORIO 
    return NULL;
}

//RETORNA O TAMANHO DO ARQUIVO QUE PRECISA SER ORDENADO
long int getTamanhoArquivo(Processo *processo)
{
    FILE* fp = fopen(processo->sArquivo, "r"); 
  
    if (fp == NULL) 
        return -1; 
  
    fseek(fp, 0L, SEEK_END); 
  
    long int tam = ftell(fp); 
 
    fclose(fp); 
  
    return tam/4; 
}

void lerArquivo(Processo *processo, int *vetor)
{
    FILE *file;
    int i = 0;

    if( (file = fopen(processo->sArquivo, "rb")) == NULL)
    {
        printf("ERRO: Arquivo não pode ser aberto (processo %d)\n", processo->my_rank);
        return;
    }

    while(!feof(file))
    {
        if( fread(& (* (vetor + i ) ), sizeof(int), 1, file) != 1 )
        {
            if(feof(file)) break;
            printf("Erro de leitura do arquivo %s", processo->sArquivo);
        }

        i++;
    }

    fclose(file);
}

//ESCREVE VETOR RECEBIDO
void imprimeVetor(int *vetor, int tamanho)
{
    int i;

    printf("[");
    for(i = 0; i < tamanho; i++)
        printf("%d, ", vetor[i]);
    printf("]\n");
}

//IMPLEMENTACAO DA FUNCAO POW COM UM CASTING PARA int 
int powCast(int b, int e)
{
    if(e == 0)
        return 1;

    int i;
    int p = b;
    e--;

    for(i = 0; i < e; i++)
        p *= b;
    
    return p;
}

// COPIA VALORES DO VETOR SOURCE PARA O VETOR DEST
int copiaVetor(int *dest, int *source, int tamanho)
{
    int i;
    for(i = 0; i < tamanho; i++)
        dest[i] = source[i];
    return +1;
}

//RETORNA O TAMANHO DA MENSAGEM ENVIADA AO PROCESSO
int getMensagemLength(Processo *processo)
{
    MPI_Status status;
    int received;          // FLAG QUE INFORMA QUANDO UMA MENSAGEM FOI ENVIADA
    int amount;            // TAMANHO DA MENSAGEM

    do
        MPI_Iprobe(processo->parceiro_rank,  MPI_ANY_TAG, MPI_COMM_WORLD, &received, &status);
    while(!received);

    MPI_Get_count(&status, MPI_INT, &amount);
    return amount;
}

//FUNCAO QUE COMPARA DOIS ELEMENTOS
int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

// MERGE AS DUAS LISTAS, REUNI AS DUAS LISTAS EM UMA LISTA ORDENADA USANDO QUICKSORT
int merge(Processo *processo)
{
    int i, j;

    processo->lengthList = processo->lengthLowList + processo->lengthHighList;
    if(processo->lengthList > processo->sizeList)
    {
        free(processo->list);
        processo->list = (int *) malloc(processo->lengthList * sizeof(int));
        processo->sizeList = processo->lengthList;
    }

    j = 0;

    if(processo->lengthLowList > 0)
    {
        for(i = 0; i < processo->lengthLowList; i++)
            processo->list[j++] = processo->lowList[i];
    }

    if(processo->lengthHighList > 0)
    {
        for(i = 0; i < processo->lengthHighList; i++)
            processo->list[j++] = processo->highList[i];
    }

    //quickSort(processo->list, 0, processo->lengthList-1);
    qsort(processo->list, processo->lengthList, sizeof(int), cmpfunc);

    return +1;
}

//SEPARA A LISTA ENTRE NUMEROS MAIORES (HIGHLIST) E MENORES (LOWLIST) QUE O PIVO
int partitionList(Processo *processo)
{
    if(processo->lengthList <= 0)
        return -1;

    int fim = processo->lengthList - 1;
    int i = 0;

    while(processo->list[i] <= processo->pivo && i <= fim)
        i++;

    processo->lengthLowList = i;
    processo->lengthHighList = processo->lengthList - i;

    if(processo->lengthLowList <  0)
        processo->lengthLowList = 0;
    else if(processo->lengthLowList > processo->sizeLowList)
    {
        free(processo->lowList);
        processo->lowList = (int *) malloc(processo->lengthLowList * sizeof(int));
        processo->sizeLowList = processo->lengthLowList;
    }

    if(processo->lengthHighList < 0)
        processo->lengthHighList = 0;
    else if(processo->lengthHighList > processo->sizeHighList)
    {
        free(processo->highList);
        processo->highList = (int *) malloc(processo->lengthHighList * sizeof(int));
        processo->sizeHighList = processo->lengthHighList;   
    }

    int m;
    int k = 0;

    for(m = 0; k < i; k++, m++)
        processo->lowList[m] = processo->list[k];  //LISTA COM NUMEROS MENORES QUE O PIVO

    for(m = 0; k <= fim; k++, m++)
        processo->highList[m] = processo->list[k]; //LISTA COM NUMEROS MAIORES QUE O PIVO

    return +1;
}

void quickMerge(Processo *processo)
{
    int *splitter = (int *) malloc(processo->comm_sz * sizeof(int));        // VETOR DE PIVOS
    int dim = (int) (log(processo->comm_sz)/log(2));                        // DIMENSAO DO HIPERCUBO
    int *vetor = NULL;                                                      // LISTA DE TODOS OS NUMEROS QUE SERAO ORDENADOS
    int i, k;
    int index;                                                              // INDICE DO VETOR DE PIVOS (SPLITTER)
    int lengthMensagem;                                                     // TAMANHO DA MENSAGEM RECEBIDA

    if(processo->my_rank == processo->root_rank)
    {
        //processo->sArquivo = getNomeArquivo();
        processo->qtdeElementos = getTamanhoArquivo(processo);

        //printf("quantidade de elementos: %d numeros\n", processo->qtdeElementos);

        processo->elementosPorProcesso = (int) ceil(processo->qtdeElementos/processo->comm_sz);

        vetor = (int *) malloc(processo->qtdeElementos * sizeof(int));

        printf("Lendo elementos (processo %d)\n", processo->my_rank);

        lerArquivo(processo, vetor);

        printf("Elementos lidos!\n");
    }

    double start, end;
    MPI_Barrier(processo->Comm);
    start = MPI_Wtime();

    //DISTRIBUI-SE A QUANTIDADE DE ELEMENTOS POR PROCESSO ENTRE OS PROCESSOS
    MPI_Bcast(&processo->elementosPorProcesso, 1, MPI_INT, processo->root_rank, processo->Comm);

    alocaMemoria(processo);

    //DIVIDE-SE A LISTA DE ELEMENTOS ENTRE OS PROCESSOS
    MPI_Scatter(vetor, processo->elementosPorProcesso, MPI_INT, processo->list, processo->elementosPorProcesso, MPI_INT, processo->root_rank, processo->Comm);
    
    processo->lengthList = processo->elementosPorProcesso;

    //ORDENA A PRIMEIRA SUBLISTA RECEBIDA PELO ROOT, USANDO QUICKSORT SEQUENCIAL
    //quickSort(processo->list, 0, processo->lengthList-1); 
    qsort(processo->list, processo->lengthList, sizeof(int), cmpfunc);

    // GERA-SE O VETOR DE PIVOS QUE SERA USADO EM TODO ALGORITMO
    if(processo->my_rank == processo->root_rank)
    {
        int n = powCast(2,dim);
        for(i = 1; i <= n - 1; i++)
            splitter[i] = vetor[(i*processo->elementosPorProcesso)/n]; 
    }

    //DISTRIBUI-SE O SPLITTER ENTRE OS PROCESSOS
    MPI_Bcast(splitter, processo->comm_sz, MPI_INT, processo->root_rank, processo->Comm);

    if(vetor != NULL)
        free(vetor);

    /*  
        EXECUTA-SE A ORDENAÇÃO 
        A TROCA DE LISTAS ENTRE OS PROCESSOS OCORRE N VEZES, N = DIMENSAO DO HIPERCUBO 
    */
    for(i = dim-1, k = 0; i >= 0; i--, k++)
    {
        // DEFINE-SE O PARCEIRO PARA A TROCA DE LISTAS
        processo->parceiro_rank = processo->my_rank ^ powCast(2,i); 
        
        // DEFINE-SE O INDEX DO SPLITTER 
        index = (processo->my_rank & (powCast(2,dim)-powCast(2,dim-k))) | powCast(2, dim-k-1);

        processo->pivo = splitter[index]; //PROCESSO RECEBE O PIVO

        //SEPARA A LISTA ENTRE NUMEROS MENORES E MAIORES QUE O PIVO
        partitionList(processo);

        /*
                ENVIA LISTA COM NUMEROS MENORES QUE O PIVO PARA 
            PARCEIRO COM MENOR RANK
                ENVIA LISTA COM NUMEROS MAIORES QUE O PIVO PARA
            PARCEIRO COM MAIOR RANK
        */
        if(processo->my_rank > processo->parceiro_rank)
        {
            MPI_Send(&processo->lowList[0], processo->lengthLowList, MPI_INT, processo->parceiro_rank, 0, processo->Comm); 

            processo->lengthLowList = getMensagemLength(processo); 

            if(processo->lengthLowList > processo->sizeLowList)
            {
                free(processo->lowList);
                processo->lowList = (int *) malloc(processo->lengthLowList * sizeof(int) );
                processo->sizeLowList = processo->lengthLowList;
            }

            MPI_Recv(&processo->lowList[0], processo->lengthLowList, MPI_INT, processo->parceiro_rank, 0, processo->Comm, MPI_STATUS_IGNORE);
        }
        else
        {
            lengthMensagem = getMensagemLength(processo);

            if(lengthMensagem > processo->vetorTempLength)
            {
                free(processo->vetorTemp);
                processo->vetorTemp = (int *) malloc(lengthMensagem * sizeof(int));  
                processo->vetorTempLength = lengthMensagem;  
            } 

            MPI_Recv(&processo->vetorTemp[0], lengthMensagem, MPI_INT, processo->parceiro_rank, 0, processo->Comm, MPI_STATUS_IGNORE);

            MPI_Send(&processo->highList[0], processo->lengthHighList, MPI_INT, processo->parceiro_rank, 0, processo->Comm); 

            processo->lengthHighList = lengthMensagem;

            if(processo->lengthHighList > processo->sizeHighList)
            {
                free(processo->highList);
                processo->highList = (int *) malloc( processo->lengthHighList * sizeof(int) );
                processo->sizeHighList = processo->lengthHighList;    
            }
            
            copiaVetor(processo->highList, processo->vetorTemp, lengthMensagem);
        }

        merge(processo);
    }

    MPI_Barrier(processo->Comm); 
    end = MPI_Wtime();

    salvarArquivo(processo);

    if(processo->my_rank == processo->root_rank)
        printf("\n\nTEMPO DE ORDENACAO: %f segundos\n", end - start);
}
