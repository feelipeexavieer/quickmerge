#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <dirent.h>

char *getNomeArquivo()
{
    struct dirent *de;   
  
    // DIRETORIO DO PROGRAMA
    DIR *dr = opendir("."); 
  
    if (dr == NULL)  
        return NULL; 
  
    while ((de = readdir(dr)) != NULL) 
    { 
        if(strstr(de->d_name, "ordenacao_") != NULL)
            return de->d_name;
    }
    
    closedir(dr); //FECHA DIRETORIO 
    return NULL;
}

int getTamanhoArquivo(char *sArquivo)
{
    char *substr = strndup(sArquivo+10, strlen(sArquivo)-5);

    return atoi(substr); 
}

int removeFiles()
{
    char *sArquivo;

    for(;;)
    {
        sArquivo = getNomeArquivo();
        if(sArquivo != NULL)
            remove(sArquivo);
        else 
            break;
    }

    return +1;
}

int salvarNumeros(char *sArquivo, int qtdeNumeros, int intervalo)
{
     if(!sArquivo)
        return -1;

    FILE* arquivo = fopen(sArquivo,"wb");

    if(arquivo == NULL)
        return -4;

    int i;
    int numero;

    for(i=0; i<qtdeNumeros; i++)
    {
        //fprintf(arquivo, " %10d ", rand() % (intervalo+1));
        numero = rand() % (intervalo+1);
        fwrite(&numero, sizeof(int), 1, arquivo);
    }

    fclose(arquivo);
    return +1;
}

int powCast(int b, int e)
{
    if(e == 0)
        return 1;

    int i;
    int p = b;
    e--;

    for(i = 0; i < e; i++)
        p *= b;
    
    return p;
}

int main()
{   
    int qtdeNumeros = 0, intervalo = 0; 

    printf("Quantidade de numeros = 2^x. Informe x: ");
    scanf("%d", &qtdeNumeros);

    qtdeNumeros = powCast(2,qtdeNumeros);

    if(qtdeNumeros <= 0)
    {
        printf("Erro: Quantidade de numeros menor ou igual a 0.\n");
        return -1;
    }

    printf("Informe o intervalo: ");
    scanf("%d", &intervalo);

    if(intervalo <= 0)
    {
        printf("Erro: Intervalo menor ou igual a 0.\n");
        return -2;
    }

    //removeFiles();

    char file_name[100];
    sprintf(file_name,"ordenacao_%d.dat", qtdeNumeros);

    if(salvarNumeros(file_name, qtdeNumeros, intervalo) == +1)
    {
    	printf("%d numeros gerados com sucesso.\n", qtdeNumeros);
        printf("nome arquivo: %s\n", getNomeArquivo());
        printf("quantidade: %d\n", getTamanhoArquivo(file_name));
    }
    else
    	printf("erro ao gerar numeros!\n");
	
    return 0;
}

